//from the discussion demo
var gl;
var canvas;
var shaderProgram;

//matrix to manipulate affine
var mvMatrix = mat4.create();

//added by me
//arr/buffers for upper blue part
var blueTopVertices,blueTopVertexPositionBuffer, blueTopVertexColorBuffer;
//arr/buffers for bottom orange part
var orangeBottomVertices,orangeBottomVertexPositionBuffer, orangeBottomVertexColorBuffer;

var t = 0; // time variable for animation and parametric equations
var dt = 0.01; // difference in time between animate()

//set uniform attributes to modify
function setMatrixUniforms() {
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//convert degrees to radians
function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//from discussion, look for viewport
function createGLContext(canvas) {
	var names = ["webgl", "experimental-webgl"];
	var context = null;
	for (var i=0; i < names.length; i++) {
		try {
			context = canvas.getContext(names[i]);
		} catch(e) {}
		if (context) {
			break;
		}
	}
	if (context) {
		context.viewportWidth = canvas.width;
		context.viewportHeight = canvas.height;
	} else {
		alert("Failed to create WebGL context!");
	}
	return context;
}

//from discussion
//get element of the shader created in the html using dom
function loadShaderFromDOM(id) {
	var shaderScript = document.getElementById(id);
	
	// If we don't find an element with the specified id
	// we do an early exit 
	if (!shaderScript) {
		return null;
	}
	
	// Loop through the children for the found DOM element and
	// build up the shader source code as a string
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}
 
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}
 
	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);
 
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	} 
	return shader;
}

//get shaders, then link GL to those shaders
function setupShaders() {
	vertexShader = loadShaderFromDOM("shader-vs");
	fragmentShader = loadShaderFromDOM("shader-fs");
	
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Failed to setup shaders");
	}

	gl.useProgram(shaderProgram);
	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
	gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);
	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	
}

//set up buffers for blue top part
//use 0-23x0-23 grid, normalizes to (-1,1)
function setupBlueTopBuffers(){
	var gridSize = 24; //will scale down by 24

	//set up blue top vertex positions
	blueTopVertices = [
		//1
		1,22,0,
		1,18.5,0,
		3,18.5,0,
		//2
		1,22,0,
		3,18.5,0,
		7.25,18.5,0,
		//3
		1,22,0,
		7.25,18.5,0,
		11,22,0,
		//4
		7.25,18.5,0,
		11,22,0,
		14.75,18.5,0,
		//5
		11,22,0,
		21,22,0,
		14.75,18.5,0,
		//6
		14.75,18.5,0,
		21,22,0,
		19,18.5,0,
		//7
		21,22,0,
		19,18.5,0,
		21,18.5,0,
		//8
		3,18.5,0,
		7.25,18.5,0,
		7.25,15.5,0,
		//9
		3,18.5,0,
		7.25,15.5,0,
		3,13,0,
		//10
		7.25,15.5,0,
		3,13,0,
		7.25,11,0,
		//11
		3,13,0,
		7.25,11,0,
		3,8,0,
		//12
		7.25,11,0,
		3,8,0,
		7.25,8,0,
		//13
		7.25,15.5,0,
		7.25,11,0,
		9,15.5,0,
		//14
		7.25,11,0,
		9,15.5,0,
		9,11,0,
		//15
		14.75,18.5,0,
		19,18.5,0,
		14.75,15.5,0,
		//16
		19,18.5,0,
		14.75,15.5,0,
		19,13,0,
		//17
		14.75,15.5,0,
		19,13,0,
		14.75,11,0,
		//18
		19,13,0,
		14.75,11,0,
		19,8,0,
		//19
		14.75,11,0,
		19,8,0,
		14.75,8,0,
		//20
		14.75,15.5,0,
		14.75,11,0,
		13,15.5,0,
		//21
		14.75,11,0,
		13,15.5,0,
		13,11,0
	];
	blueTopVertices = blueTopVertices.map(function(x) {return ((x*2.0)/gridSize)-1;}); //scale down vertices

	//set up blue top pos buffers
	blueTopVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, blueTopVertexPositionBuffer); // work on blue pos buffer
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(blueTopVertices), gl.STATIC_DRAW); //send blue array verts to blu vert buffer
	blueTopVertexPositionBuffer.itemSize = 3;
	blueTopVertexPositionBuffer.numberOfItems = 21*3;

	//set up colors for each vertex
	var blueTopColors = [];
	for(var i = 0; i < blueTopVertices.length / 3; i++){ // same color everywhere
		blueTopColors.push(19/255.0);
		blueTopColors.push(40/255.0);
		blueTopColors.push(75/255.0);
		blueTopColors.push(1.0); // color for the blue top part in parts of 255
	}

	//set up blue top color buffers
	blueTopVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, blueTopVertexColorBuffer); // work on blue col buffer
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(blueTopColors), gl.STATIC_DRAW); //send blue array cols to blu vert buffer
	blueTopVertexColorBuffer.itemSize = 4;
	blueTopVertexColorBuffer.numItems = 21;
}

//set up buffers for blue top part
//use 0-23x0-23 grid, normalizes to (-1,1)
function setupOrangeBottomBuffers(){
	var gridSize = 24; //will scale down by 24

	orangeBottomVertices = [
		//1
		3,7,0,
		3,6,0,
		4.33,7,0,
		//2
		3,6,0,
		4.33,7,0,
		4.33,4.8,0,
		//3
		6,7,0,
		7.25,7,0,
		6,4,0,
		//4
		7.25,7,0,
		6,4,0,
		7.25,3,0,
		//5
		8.8,7,0,
		10.25,7,0,
		8.8,2,0,
		//6
		10.25,7,0,
		8.8,2,0,
		10.25,0.9,0,
		//7
		11.75,0.9,0,
		13.2,2,0,
		11.75,7,0,
		//8
		13.2,2,0,
		11.75,7,0,
		13.2,7,0,
		//9
		14.7,3,0,
		16,4,0,
		14.7,7,0,
		//10
		16,4,0,
		14.7,7,0,
		16,7,0,
		//11
		17.66,4.8,0,
		17.66,7,0,
		19,6,0,
		//12
		17.66,7,0,
		19,6,0,
		19,7,0
	];
	orangeBottomVertices = orangeBottomVertices.map(function(x) {return ((x*2.0)/gridSize)-1;}); //scale down vertices

	//set up orange bottom pos buffers
	orangeBottomVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, orangeBottomVertexPositionBuffer); // work on blue pos buffer
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(orangeBottomVertices), gl.STATIC_DRAW); //send blue array verts to blu vert buffer
	orangeBottomVertexPositionBuffer.itemSize = 3;
	orangeBottomVertexPositionBuffer.numberOfItems = 12*3;

	//set up colors for each vertex
	var orangeBottomColors = [];
	for(var i = 0; i < orangeBottomVertices.length / 3; i++){ // same color everywhere
		orangeBottomColors.push(233/255.0);
		orangeBottomColors.push(74/255.0);
		orangeBottomColors.push(55/255.0);
		orangeBottomColors.push(1.0); // color for the blue top part in parts of 255
	}

	//set up orange bottom color buffers
	orangeBottomVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, orangeBottomVertexColorBuffer); // work on blue col buffer
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(orangeBottomColors), gl.STATIC_DRAW); //send blue array colss to blu vert buffer
	orangeBottomVertexColorBuffer.itemSize = 4;
	orangeBottomVertexColorBuffer.numItems = 21;
}

//render the meshes with uniform attributes applied
function draw() { 
	//setup viewport
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	var affineTransformVec = vec3.create(); //vector used to apply transformation to model view matrix

	mat4.identity(mvMatrix); //reset matrix
	vec3.set(affineTransformVec, Math.sin(t), Math.sin(t), 1); // vector indicating how to scale matrix, use sin(t) with animate
	mat4.scale(mvMatrix, mvMatrix, affineTransformVec); //scale the matrix
	vec3.set(affineTransformVec, 0, 0, 1); // vector indicating axis to rotate around
	mat4.rotate(mvMatrix, mvMatrix, t, affineTransformVec) //rotate the matrix, using angle t

	//draw blue top
	gl.bindBuffer(gl.ARRAY_BUFFER, blueTopVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 
				blueTopVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, blueTopVertexColorBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, 
				blueTopVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);
	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, blueTopVertexPositionBuffer.numberOfItems);
	//draw orange bottom
	gl.bindBuffer(gl.ARRAY_BUFFER, orangeBottomVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 
				orangeBottomVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, orangeBottomVertexColorBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, 
				orangeBottomVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);
	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, orangeBottomVertexPositionBuffer.numberOfItems);

}

//make t increase by a bit.
//also make it so some of the triangles in the orange part change vertices directly.
function animate() {
	t += dt;

	var dx = 0.4*Math.cos(8*t)*Math.cos(4*t) //make some vertices change by rose curve
	var dy = 0.4*Math.cos(8*t)*Math.sin(4*t)

	orangeBottomVerticesMoved = [] //create copy of orange vertices with some vertices changed
	for (var i = 0; i < orangeBottomVertices.length; i++){
		if (i % 18 == 0) { //only change the first two vertices in each "strip"
			orangeBottomVerticesMoved.push(orangeBottomVertices[i]+dx)
		}
		else if (i % 18 == 1) {
			orangeBottomVerticesMoved.push(orangeBottomVertices[i]+dy)
		}
		else {
			orangeBottomVerticesMoved.push(orangeBottomVertices[i])
		}

	}
	
	//work on orange bottom buffer, and change that data with new changees
	gl.bindBuffer(gl.ARRAY_BUFFER, orangeBottomVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(orangeBottomVerticesMoved), gl.STATIC_DRAW);
}

//call this in html, set up everything, and start the animation
function startup() {
	canvas = document.getElementById("myGLCanvas");
	gl = createGLContext(canvas);
	setupShaders(); 
	setupBlueTopBuffers();
	setupOrangeBottomBuffers();
	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	gl.enable(gl.DEPTH_TEST);
	tick();	
}

//tick called on next repaint. draw again with parameters changed by animate()
function tick() {
	requestAnimFrame(tick);
	draw();
	animate();
}
