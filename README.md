# UIUC-logo-animation

(Implementation of MP1 in CS418 at UIUC)

Showcase of 2D affine transformations on the UIUC logo as a whole, and non-affine transformations on the strips (using the rose curve) using WebGL.

To run, simply run mp1.html in a web browser.

![UIUC Logo](/sample/UIUC-animation.gif)
